package com.training;

import com.training.pets.Dragon;

public class Main {
    public static void main(String[] args) {
        Feedable[] feedableThings = new Feedable[5];
        feedableThings[0] = new Dragon("Spyro", 4);
        feedableThings[3] = new Person("Conor", 26);

        for (int i = 0; i < feedableThings.length; i++) {
            if (feedableThings[i] != null) {
                feedableThings[i].feed();
            }

            if (feedableThings[i] instanceof Dragon) {
                ((Dragon) feedableThings[i]).breathe();
            }
        }
    }
}
