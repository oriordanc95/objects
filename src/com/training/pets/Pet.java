package com.training.pets;

import com.training.Feedable;

public abstract class Pet implements Feedable {
    private static int totalPets;

    private String name;
    private int numLegs;

    public abstract void feed();

    public Pet() {
        totalPets++;
    }

    public Pet(String name, int numLegs) {
        this();
        this.name = name;
        this.numLegs = numLegs;
    }

    public static int getTotalPets() {
        return totalPets;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumLegs() {
        return numLegs;
    }

    public void setNumLegs(int numLegs) {
        this.numLegs = numLegs;
    }
}
