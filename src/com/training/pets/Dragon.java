package com.training.pets;

public class Dragon extends Pet{
    private String typeOfBreath;

    public Dragon() {
        this(null, 4, "fire");
    }

    public Dragon(String name, int numLegs) {
        this(name, numLegs, "fire");
    }

    public Dragon(String name, int numLegs, String typeOfBreath) {
        super(name, numLegs);
        this.typeOfBreath = typeOfBreath;
    }

    @Override
    public void feed() {
        System.out.println("Fed the dragon some dragon food");
    }

    public void breathe() {
        System.out.println("Dragon breathed " + this.typeOfBreath);
    }

}
